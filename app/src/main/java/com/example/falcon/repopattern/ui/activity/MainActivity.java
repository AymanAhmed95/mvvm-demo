package com.example.falcon.repopattern.ui.activity;

import android.arch.lifecycle.ViewModelProviders;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;

import com.example.falcon.repopattern.App;
import com.example.falcon.repopattern.R;
import com.example.falcon.repopattern.data.entity.Author;
import com.example.falcon.repopattern.data.repository.AuthorRepository;
import com.example.falcon.repopattern.ui.adapter.AuthorAdapter;

import java.util.ArrayList;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.functions.Consumer;

public class MainActivity extends AppCompatActivity {

    public static final String TAG = MainActivity.class.getSimpleName();
    Unbinder unbinder;
    @Inject
    AuthorRepository authorRepository;

    @BindView(R.id.authors_recycler)
    RecyclerView authorsRecycler;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ((App) getApplication()).getAuthorComponent().inject(this);
        unbinder = ButterKnife.bind(this);
        MainViewModelFactory factory = new MainViewModelFactory(authorRepository);
        MainViewModel vmodel = ViewModelProviders.of(this,factory).get(MainViewModel.class);
        Author author = new Author();
        author.setName("just my test");
        authorRepository.addAuthor(author);

        final AuthorAdapter authorAdapter = new AuthorAdapter(new ArrayList<Author>(),this);
        authorsRecycler.setAdapter(authorAdapter);
        authorsRecycler.setLayoutManager(new LinearLayoutManager(this));

        vmodel.getAuthorsList().observeOn(AndroidSchedulers.mainThread()).subscribe(new Consumer<Author>() {
            @Override
            public void accept(Author author){
                Log.d(TAG, "accept: " + author.getName());
                authorAdapter.addAuthorToList(author);
            }
        });
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
        unbinder.unbind();
    }
}
