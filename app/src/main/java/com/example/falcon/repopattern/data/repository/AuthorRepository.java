package com.example.falcon.repopattern.data.repository;

import android.util.Log;

import com.example.falcon.repopattern.data.AppDB;
import com.example.falcon.repopattern.data.entity.Author;

import org.reactivestreams.Publisher;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.TimeUnit;

import io.reactivex.Completable;
import io.reactivex.Flowable;
import io.reactivex.functions.Action;
import io.reactivex.functions.Function;
import io.reactivex.functions.Predicate;
import io.reactivex.schedulers.Schedulers;

public class AuthorRepository {

    AppDB appDB;

    public AuthorRepository(AppDB appDB) {
        this.appDB = appDB;
    }

    public void addAuthor(final Author author){
        Completable.fromAction(new Action() {
            @Override
            public void run(){
                    appDB.authorDao().addAuthor(author);
            }
        }).subscribeOn(Schedulers.io()).subscribe();
    }

    public Flowable<Author> getAllAuthors(){
        return appDB.authorDao().getAllAuthors().subscribeOn(Schedulers.io()).flatMap(new Function<List<Author>, Flowable<Author>>() {
            @Override
            public Flowable<Author> apply(List<Author> authors) {
                return  Flowable.fromIterable(authors);
            }
        });
    }
}
