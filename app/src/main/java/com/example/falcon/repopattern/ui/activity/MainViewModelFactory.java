package com.example.falcon.repopattern.ui.activity;

import android.arch.lifecycle.ViewModel;
import android.arch.lifecycle.ViewModelProvider;
import android.support.annotation.NonNull;

import com.example.falcon.repopattern.data.repository.AuthorRepository;

public class MainViewModelFactory implements ViewModelProvider.Factory {

    AuthorRepository authorRepository;

    public MainViewModelFactory(AuthorRepository authorRepository) {
        this.authorRepository = authorRepository;
    }

    @NonNull
    @Override
    public <T extends ViewModel> T create(@NonNull Class<T> modelClass) {
        if(modelClass.isAssignableFrom(MainViewModel.class)){
            return (T) new MainViewModel(authorRepository);
        }
        throw new IllegalArgumentException("Unknown Model Class");
    }
}
