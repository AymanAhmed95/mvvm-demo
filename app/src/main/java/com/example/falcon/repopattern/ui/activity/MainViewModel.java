package com.example.falcon.repopattern.ui.activity;

import android.arch.lifecycle.ViewModel;

import com.example.falcon.repopattern.data.entity.Author;
import com.example.falcon.repopattern.data.repository.AuthorRepository;
import io.reactivex.Flowable;
import io.reactivex.functions.Predicate;
import io.reactivex.schedulers.Schedulers;

public class MainViewModel extends ViewModel {

    AuthorRepository repo;

    public MainViewModel(AuthorRepository repo) {
        this.repo = repo;
    }


    public Flowable<Author> getAuthorsList(){
        return repo.getAllAuthors().filter(new Predicate<Author>() {
            @Override
            public boolean test(Author author){
                return author.getId() % 2 == 0;
            }
        }).observeOn(Schedulers.io());
    }
}
